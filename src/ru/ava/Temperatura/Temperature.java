package ru.ava.Temperatura;

import java.util.Scanner;

/**
 * Класс для расчета средней температуры за месяц
 */

public class Temperature {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double[] temperature = new double[numberOfDays];


        fillTemperature(temperature);
        double max = findMax(temperature);
        double min = findMin(temperature);
        warmDays(temperature, max);
        coldDays(temperature, min);

        System.out.print("Средняя температура за месяц - ");
        System.out.printf("%5.1f%n", averageTemperature(temperature));
        System.out.print("Максимальная температура за месяц - ");
        System.out.printf(" %1.1f%n ", max);
        System.out.print("Минимальная температура за месяц - ");
        System.out.printf(" %1.1f%n ", min);
        System.out.printf("%nДни с максимальной температурой: ");
        outArray(warmDays(temperature, max));
        System.out.printf("%nДни с минимальной температурой: ");
        outArray(coldDays(temperature, min));
    }

    /**
     * Метод для генерации температуры в каждый день
     *
     * @param temperature температура дня
     */
    private static void fillTemperature(double[] temperature) {
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = -5 + (Math.random() * 25);
            System.out.printf("%1.1f%n ", temperature[i]);
        }

    }

    /**
     * Метод для вычисления средней температуры за всё кол-во дней*
     *
     * @param temperature все температуры
     *
     * @return Средняя температура
     */
    private static double averageTemperature(double[] temperature) {
        double sum = 0.0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }

        return sum / temperature.length;

    }

    /**
     * Метод для нахождения максимальной температуры
     *
     * @param temperature массив температур
     *
     * @return максимальную температуру
     */
    private static double findMax(double[] temperature) {
        double max = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] > max) {
                max = temperature[i];
            }
        }
        return max;

    }

    /**
     *Метод для нахождения минимальной температуры
     *
     * @param temperature
     *
     * @return минимальную температуру
     */
    private static double findMin(double[] temperature) {
        double min = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] < min) {
                min = temperature[i];
            }
        }
        return min;
    }

    /**
     * Метод для нахождения количества самых теплых дней
     *
     * @param temperature массив температур
     *
     * @return массив теплых дней
     */
    private static int warmDays(double temperature[]) {
        int day = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == findMax(temperature)) {
                day++;
            }
        }
        return day;
    }

    /**
     * Метод для нахождения номера теплого дня
     *
     * @param temperature массив температур
     * @param max максимальное значение
     *
     * @return массив номеров теплых дней
     */
    private static int[] warmDays(double[] temperature,double max) {
        int[] day = new int[warmDays(temperature)];
        int j = -1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == max) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }

    /**
     *  Метод для нахождения количества самых холодных дней
     *
     * @param temperature массив температур
     *
     * @return массив холодных дней
     */
    private static int coldDays(double temperature[]) {
        int day = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == findMin(temperature)) {
                day++;
            }
        }
        return day;
    }

    /**
     * Метод для нахождения номера холодного дня
     *
     * @param temperature массив температур
     * @param min минимальное значение
     *
     * @return массив номеров холодных дней
     */
    private static int[] coldDays(double[] temperature,double min) {
        int[] day = new int[coldDays(temperature)];
        int j = -1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == min) {
                j++;
                day[j] = i + 1;
            }
        }
        return day;
    }

    /**
     * Метод вывода массива дней с пробелом
     *
     * @param days массив дней
     */
    private static void outArray(int[] days) {
        for (int i = 0; i < days.length; i++) {
            System.out.print(days[i] + " ");
        }
    }
}


