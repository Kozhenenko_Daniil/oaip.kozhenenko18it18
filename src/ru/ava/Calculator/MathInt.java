package ru.ava.Calculator;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import sun.awt.geom.AreaOp;

import javax.sound.midi.Soundbank;

/**
 * @author Kozhenenko D.D.
 */
public class MathInt {
    /**
     * Метод для вычисления суммы операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение суммы
     */
    public static int sum(int x, int y) {

        return x + y;

    }

    /**
     * Метод для вычисления разности операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение разницы
     */
    public static int min(int x, int y) {

        return x - y;

    }

    /**
     * Метод для вычисления произведения операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение произведения
     */

    public static int umn(int x, int y) {

        return x * y;

    }

    /**
     * Метод для вычисления частного операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение частного
     */
    public static int del(int x, int y) {

        return x / y;

    }

    /**
     * Метод для вычисления остатка от деления операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение остатка
     */
    public static int ost(int x, int y) {

        return x % y;

    }

    /**
     * Метод для вычисления степени операндов
     *
     * @param x 1 операнд
     * @param y 2 операнд
     * @return значение степени
     */
    public static double stepen(int x, int y) {

        if (y == 0) {
            return 1;

        }
        double result = x;
        if (y < 0) {
            result = 1.0 / (result * (-y));
        } else {
            for (int i = 1; i < y; i++) {
                result = result * x;
            }
            return result;
        }

        for (int i = 1; i <= y; i++) {
            result = result * x;
        }

        return result;


    }
}
