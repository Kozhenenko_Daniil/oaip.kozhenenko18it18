package ru.kdd.patient;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Patient patientOne = new Patient("Ivanov", 1980, 1, true);
        Patient patientTwo = new Patient("Sidorov", 1990, 2, false);
        Patient patientThree = new Patient("Petrov", 1995, 3, false);
        Patient patientFour = new Patient("Vasin", 2000, 4, true);
        Patient patientFive = new Patient("Fedotov", 2002, 5, false);

        System.out.println(patientOne.toString());
        System.out.println(patientTwo.toString());
        System.out.println(patientThree.toString());
        System.out.println(patientFour.toString());
        System.out.println(patientFive.toString());





    }
}