package ru.kdd.songs;

/**
 * Класс представления характеристик песни
 *
 * @author Kozhenenko D.D
 */
public class Song {
    private String nameOfSong;
    private String artistOfSong;
    private int duration;

    Song(String song, String executor, int duration) {
        this.nameOfSong = song;
        this.artistOfSong = executor;
        this.duration = duration;

    }



    int getDuration() {
        return duration;
    }

    /**
     * Позволяет определить к какой категории продолжительности относится песня
     *
     * @return категорию продолжительности песни
     */
    String category() {
        String songLenght = "";

        if (duration < 120) {
            songLenght = "short";
        }

        if (duration > 120 && (duration < 240)) {
            songLenght = "medium";
        }

        if (duration > 240) {
            songLenght = "long";
        }
        return songLenght;
    }

    /**
     * Проверяет относиться ли текущий объект и полученный к одной и той же категории
     *
     * @param song песню
     *
     * @return true,если категории совпадают,иначе false
     */
    boolean isSameCategory(Song song) {
        return song.category().equals(this.category());
    }

    @Override
    public String toString() {
        return "Song{" +
                "song='" + nameOfSong + '\'' +
                ", executor='" + artistOfSong + '\'' +
                ", duration=" + duration +
                '}';
    }
}