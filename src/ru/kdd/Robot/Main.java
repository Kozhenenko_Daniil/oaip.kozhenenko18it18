package ru.kdd.Robot;

import java.util.Scanner;

/**
 * Класс для вывода конечной позиции и взгляда робота
 *
 * @author  Kozhenenko D.D
 */
public class Main {
    private static Scanner scanner=new Scanner(System.in);

    public static void main(String[] args) {


        Robot robot = input();
        System.out.println(robot);

        System.out.print("Введите конечную позицию x:");
        int endX = scanner.nextInt();
        System.out.print("Введите конечную позицию y:");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }


    /**
     * Получение начальных характеристик робота
     *
     * @return
     */
    private static Robot input() {
        Directoria directionNow = Directoria.UP;
        System.out.print("Введите начальную позицию x:");
        int x = scanner.nextInt();
        System.out.print("Введите начальную позицию y:");
        int y = scanner.nextInt();
        System.out.print("Введите изначальное направление взгляда/вверх, вниз, вправо, влево/:");
        scanner.nextLine();
        String lookAt = scanner.nextLine();

        switch (lookAt) {
            case "вверх":
                directionNow = Directoria.UP;
                break;
            case "вниз":
                directionNow = Directoria.DOWN;
                break;
            case "вправо":
                directionNow = Directoria.RIGHT;
                break;
            case "влево":
                directionNow = Directoria.LEFT;
                break;
            default:
                break;
        }
        return new Robot(x, y, directionNow);

    }
}






