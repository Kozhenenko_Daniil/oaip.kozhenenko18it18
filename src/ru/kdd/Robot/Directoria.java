package ru.kdd.Robot;

/**
 * Содержит директорию взглядов робота
 *
 * @autor Kozhenenko D.D.
 */
public enum Directoria {
    UP,
    RIGHT,
    DOWN,
    LEFT;
}

