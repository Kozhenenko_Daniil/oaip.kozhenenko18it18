package ru.kdd.bankAccount;

public class Account {
    private double balance;
    private double rate;

    Account(double balance, double rate) {
        this.balance = balance;
        this.rate = rate;
    }

    double getBalance() {
        return balance;
    }

    double getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "Аккаунт {" +
                "Баланс: =" + balance +
                ", Процентная ставка: =" + rate +
                '}';
    }
}
