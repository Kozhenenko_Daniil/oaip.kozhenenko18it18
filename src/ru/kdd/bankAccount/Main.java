package ru.kdd.bankAccount;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, показывающий возможности работы с объектами класса BankAccount
 *
 * @author Kozhenenko D.D
 */
public class Main {
    public static void main(String[] args) {
        Account account1 = new Account(45000, 12.4);
        Account account2 = new Account(100000, 9.6);
        Account account3 = new Account(950000, 7.3);
        Account account4 = new Account(100, 15.8);
        Account account5 = new Account(4000000, 4);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        accounts.add(account4);
        accounts.add(account5);
        accounts.add(inputClient());
        profit(accounts);

    }

    private static Account inputClient() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите баланс клиента  -  ");
        double balance = scanner.nextDouble();
        System.out.print("Введите процентную ставку - ");
        double rate = scanner.nextDouble();
        return new Account(balance, rate);
    }

    /**
     * Выводит состояние банковского счета с учетом процентных начислений за один год
     *
     * @param accounts список аккаунтов
     */
    private static void profit(ArrayList<Account> accounts) {
        double a;
        for (Account bankAccount : accounts) {
            a = bankAccount.getBalance() + bankAccount.getBalance() * bankAccount.getRate();
            System.out.println("Состояние счета " + bankAccount + " через год: " + a);


        }
    }
}
