package ru.kdd.forms;

/**
 * Содержит возможные цвета фигур
 *
 * @author Kozhenenko D.D
 */
public enum Color {
    WHITE,
    BLACK,
    RED,
    GREEN,
    BLUE,
    YELLOW,
}

