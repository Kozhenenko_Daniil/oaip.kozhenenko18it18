package ru.kdd.forms;

/**
 * Абстрактный класс для представлкния цветных фигур
 *
 * @author Kozhenenko D.D
 */

public abstract class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public abstract double area();
}
